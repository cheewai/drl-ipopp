#! /bin/bash
#
# Ensure that all IPOPP processes are running fine. Otherwise, restart IPOPP.
# Useful to be run periodically from cron
#
main_dir=$(dirname $(readlink -f $0))
env_file="${env_file:-${main_dir}/build_run_env_nogit.sh}"
source $env_file

set -eu
docker exec -u ipopp ${DOCKER_NAME} bash -c 'cd /home/ipopp/drl/tools && ./services.sh status | grep -i " not " && ./services.sh restart' 
