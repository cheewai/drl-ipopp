#! /bin/bash

if [ "$(id -u)" != "0" ] && ! (groups | grep docker >/dev/null 2>&1) ; then
  echo "Requires sudo or be in docker group" >&2
  exit 1
fi
main_dir=$(dirname $(readlink -f $0))
env_file="${env_file:-${main_dir}/build_run_env_nogit.sh}"
source $env_file

set -eu

pushd $main_dir

cat <<EOT >Dockerfile
#
# *** CHANGES TO THIS FILE WILL BE OVERWRITTEN BY 'docker build' ***
#
FROM centos:centos7
LABEL maintainer "Cheewai Lai <clai@csir.co.za>"

ARG GOSU_VERSION=1.10

#
# DRL-IPOPP requirements begins with mysql-server
# Install gosu
# Set locale
# Set timezone
#
RUN yum install -y epel-release \
 && yum -y update \
 && yum install -y \
    curl \
    less \
    wget \
    rsync \
    net-tools \
    lsof \
    unzip \
    rsyslog \
    tar \
    libxp6 \
    tcsh \
    bc \
    ed \
    # java-1.7.0-openjdk \
    ImageMagick \
    libaio \
    jemalloc \
    perl \
    ftp \
    gawk \
    libstdc++.i386 \
    hostname \
 && yum clean all \
 && curl -o gosu -kfsSL "https://github.com/tianon/gosu/releases/download/\${GOSU_VERSION}/gosu-amd64" \
 && mv gosu /usr/bin/gosu \
 && chmod +x /usr/bin/gosu \
 && localedef --no-archive -i en_US -f UTF-8 en_US.UTF-8 \
 && mv /etc/localtime /etc/localtime.ORIG \
 && ln -s /usr/share/zoneinfo/UTC /etc/localtime \
 && echo 'PS1="\[\e]0;\u@\h: \w\a\]\u@\h:\w\$ "' >> /etc/profile

#ENV JAVA_HOME /usr/lib/jvm/jre-1.7.0-openjdk.x86_64
ADD docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
EOT

docker build --rm -t $REPO_NAME:$REPO_TAG .
popd
