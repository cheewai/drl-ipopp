# NASA DRL IPOPP for MODIS and NPP Processing

This project is for running the NASA Direct Readout Laboratory - [International Polar Orbiter Processing Package IPOPP](http://directreadout.sci.gsfc.nasa.gov/) software that transforms direct-broadcast MODIS TERRA and AQUA, Suomi-NPP VIIRS, ATMS, and CrIS Raw Data Record (RDR) products to Sensor Data Record (SDR) products on Intel Linux computers. It is an alternative to the University of Wisconsin SSEC [CIMSS CSPP](http://cimss.ssec.wisc.edu/cspp/) software.

Due to the huge size of the software (tens of gigabyte), they are not included in the docker image during the build phase.

## System Requirements

- 8 cores, 64-bit processor
- 32 GB RAM (minimum)
- 64-bit Fedora 18, CentOS 6.4, OpenSuSE 12.1 or Kubuntu 13.04 64-bit
- 100 GB disk space (minimum)
- Internet connection (for downloading ancillary data).
- Use 24-hour UTC synchronized through NTP server
- Java runtime environment 1.6.0\_25 or later
- MySQL client and server 5.1 or later
- libXp 1.0 or later
- bash 4.1.2 or later
- tcsh 6.17 or later
- bc 1.06 or later
- ed 1.1 or later
- native 32-bit support or ia32-libs (ia32-libs should not be installed on systems providing native support such as RHEL, Fedora, OpenSuSE and CentOS). Additionanally, Fedora requires the 32-bit version of ld-linux.so.2 which can be obtained by installing glibc for i686
- Docker requires Linux kernel 3.8 or higher but it is widely avaialable in [various distributions](https://docs.docker.com/installation/#installation)

These requirements are irrelevant in Docker setup:

- TCP ports 20 and 21 for FTP
- TCP port 3306 for MySQL
- TCP ports 3500 through 3550 for SLS
- TCP ports 4900 through 4950 for IPOPP Data Storage Manager (DSM)
- TCP port 49152 through 65535 for passive mode FTP


[Docker container](https://www.docker.com/) is used to set up the runtime environment so that the prerequisites and dependencies are explictly and completely specified.


## Initial setup

### Download IPOPP

- Go to http://directreadout.sci.gsfc.nasa.gov/
- Register
- Download the latest IPOPP software and save it in *SRC\_DIR*

### Build docker image

- Create your own *main/build\_run\_env\_nogit.sh* 

- Copy *main/default_config.file.ORIG* to *main/default_config.file* and configure it to select the output products you want, taking into account the RAM available on your processing hardware

- Run the build script
```
> main/build
```

### Unpack and install IPOPP software during first-run

- Run a previously built docker image
```
> main/run
```

- When the docker image runs, it executes *main/start.sh*

- This script will check if the directory *HOME\_DIR/IPOPP* exists. If not, it will look for IPOPP package in *SRC\_DIR*, unpack it and run the installer script. This process can take up to 20 minutes.

Within a running instance of the container, the IPOPP software will continuously monitor /raid/dsm/nisfes\_data/ for incoming output from RT-STPS which processes raw NPP \*.dat to {RATMS-RNSCA,RCRIS-RNSCA,RNSCA-RVIRS}\*.h5.

The actual directory on the docker host for mapping as /raid/ in the container is defined by *RAID\_DIR* environment variable.

To avoid needless copying of input files from one directory to another, for example, the docker host is running FTP service in chroot environment to receive RT-STPS output from Orbital FES and the upload diretory is /data/fes/rsrunpp/fes-incoming/, we can avail this FTP receive directory directly as the IPOOP pickup directory with **/etc/fstab** entry, but write out the actual value of *$RAID\_DIR* in fstab because there is no shell to expand the variable.

```
# IPOPP-2.5 and earlier
$RAID\_DIR/dsm/nisfes_data /data/fes/rsrunpp/fes-incoming none bind
# IPOPP-2.6 or later
$RAID\_DIR/dsm/ingest /data/fes/rsrunpp/fes-incoming none bind
```

The IPOPP user guide states the the IPOPP background service expects to find incoming data files in /home/ipopp/drl/data/dsm/nisfes_data/ and it will save output to /home/ipopp/drl/data/pub/gsfcdata/. In the case with Docker, this is no longer accurate. The input directory defined by **RAID_DIR** variable is actually *mounted* and *virtualized* as /raid inside the container. This is so that the same data files will persist and remain available when the Docker container are stopped or restarted.

The additional entries made in */etc/fstab* is to map vsftpd incoming directory to the directory from which IPOPP monitors for new data.

## Run graphical IPOPP user-interface

IPOPP includes the following Java graphical interfaces:

- drl/tools/dashboard.sh
- drl/tools/sls_console.sh
- drl/dsm/gui-scripts/passmanager.sh

If the docker host is headless i.e. no X-windows running, you will need to log in to the docker host remotely from a workstation with X-windows running:

- Enable X-11 forwarding
- `ssh -X -Y` to the docker host
- If container is running, stop it.
- Run the container
- In the SSH session, `docker exec` into the container
    - source /etc/profile
    - export HOME=/home/ipopp TERM=xterm
    - source ~/.profile
    - ~/drl/tools/sls_console.sh


## Updating SPA

Sometimes, updates for individual SPA are made available after a IPOPP release and the updates will have to be installed.

- Download the *SPA*.tar.gz from DRL web site to *HOME_DIR/*
- cd $HOME/drl
- ln ../*SPA*.tar.gz .
- $HOME/drl/tools/install_spa.sh *SPA*.tar.gz
- Preconfigure SPA, if required as per user manual
- $HOME/drl/SPA/*SPA*/NISGSinstall.sh
- $HOME/drl/tools/services.sh start
- Log out from container

## SPA-specific Steps

### MODIS Burnscar
- Attach to the running container
- cd drl/SPA/burnscar/algorithm
- ./configure


## Housekeeping

- Clean up $RAID_DIR/dsm/nisfes_data for stale and unwanted files


## Directory structure

### main
Scripts and files used to build and run the Docker image

First and foremost, create *build_run_env_nogit.sh* which defines some mandatory environment variables. This file is *sourced* by the *build* and *run* script. It may contain login credentials therefore it is excluded from the Git repo.

The user (e.g. eouser) who builds and runs the docker image must have 'docker' as one of his supplementary group, i.e.
```
  sudo usermod -a -G docker eouser
```

#### Values baked into the Docker image. If changed, the image must be re*build*.
- REPO_NAME=ipopp
- REPO_TAG=2.6
- DOCKER_NAME=ipopp-modis
- APPUID=${SUDO_UID}
- SRC_DIR=*directory pathname external to container where downloaded IPOPP tgz is kept; required only if IPOPP has not been installed previously*
- HOME_DIR=*directory pathname external to container to be mounted as /home/ipopp/ at runtime*
- all **\*_DIR** directories must be local on the docker host, e.g. CIFS, NFS-mounted directories will not work

To build the Docker image:
```cd main
./build```

To run the previously built Docker image:
```cd main
./run```
